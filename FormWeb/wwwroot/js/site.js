﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.

showInPopup = (url) => {
    $.ajax(
        {
            type: "GET",
            url: url,

            beforeSend: function (xhr) {
                xhr.setRequestHeader("XSRF-TOKEN",
                    $('input:hidden[name="__RequestVerificationToken"]').val());
            },
            success: function (res) {
                $("#form-modal .modal-body").html(res);
                $("#form-modal").modal('show');
            }
        })
}

EditData = (url, para) => {
    $.ajax(
        {

            type: "GET",
            url: url,
            data: { "para": para },
            success: function (res)
            {
                $("#form-modal .modal-body").html(res);
                $("#form-modal").modal('show')
            }

        })
}
//EditData = (url,para) => {

//    $.ajax(
//        {

//            url: '/Customer/Edit?handler',
//            beforeSend: function (xhr) {
//                xhr.setRequestHeader("XSRF-TOKEN",
//                    $('input:hidden[name="__RequestVerificationToken"]').val());
//            },

//            dataType: "json",

//            data: JSON.stringify(
//                {
//                    "id": para

//                }),

//            contentType: "application/json; charset=utf-8",
//            type: "GET",
//            success: function (response)
//            {
//                $("#form-modal .modal-body").html(response);

//                $("#Code").val(response["code"]);
//                $("#Name").val(response["name"]);
//                $("#Lastname").val(response["lastname"]);
//                $("#Mobile").val(response["mobile"]);
//                $("#Job").val(response["job"]);
//                $("#Sex").val(response["sex"]);

//                $("#form-modal").modal('show');
//            },

//            error: function (data) {


//            }
//        })
//}

