﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using FormWeb.Models;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;

namespace FormWeb.Pages.LoginDefault
{
    public class RegisterModel : PageModel
    {
        private readonly FormWeb.Models.ESHOPDBContext _context;

        private readonly UserManager<IdentityUser> userManager; //Consturctor asp.net Identity For Create User

        private readonly SignInManager<IdentityUser> signInManager; //Consturctor asp.net Identity For Sign in

        public RegisterModel(FormWeb.Models.ESHOPDBContext context,
                             UserManager<IdentityUser> _userManager,
                             SignInManager<IdentityUser> _signInManager)
        {
            _context = context;
            userManager = _userManager;
            signInManager = _signInManager;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Tbluser Tbluser { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Tbluser tbluser = new Tbluser();

            tbluser = insertUser();

            IdentityUser user = new IdentityUser { UserName = tbluser.Username, PasswordHash = tbluser.Password };

            var result = await userManager.CreateAsync(user, tbluser.Password); // Insert User

            if (result.Succeeded) // ต้องเข้าเงื่อนไขของการสร้างรหัสถึงจะ สามารถบันทึกลง base ได้
            {
                await signInManager.SignInAsync(user, isPersistent: false); //เมื่อหลังจากสมัครสมาชิกเสร็จเรียบร้อย คำสั่งนี้จะ ล๊อกอินด้วย user ที่สมัคร
                _context.Tblusers.Add(tbluser);
                await _context.SaveChangesAsync();

                var login = await signInManager.PasswordSignInAsync(tbluser.Username, tbluser.Password, isPersistent: false, false);

                if (login.Succeeded)
                {
                    return RedirectToPage("/Customer/Index");
                }
            }

            return Page();
        }

        public Tbluser insertUser()
        {
            var sessionEmp = JsonConvert.DeserializeObject<Tblemployee>
                             (HttpContext.Session.GetString("SessionEmp"));

            Tbluser tblusers = new Tbluser();

            tblusers.Username = Tbluser.Username;
            tblusers.Password = Tbluser.Password;
            tblusers.EmpCode = sessionEmp.EmpCode;

            return tblusers;
        }
    }
}
