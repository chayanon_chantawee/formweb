﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using FormWeb.Models;
using FormWeb.ModelsMapping;
using Microsoft.AspNetCore.Identity;

namespace FormWeb.Pages.LoginDefault
{

    public class LoginPageModel : PageModel
    {
        private readonly FormWeb.Models.ESHOPDBContext _context;

        private readonly UserManager<IdentityUser> userManager;

        private readonly SignInManager<IdentityUser> signInManager;

        public LoginPageModel(FormWeb.Models.ESHOPDBContext context,
                             UserManager<IdentityUser> _userManager,
                             SignInManager<IdentityUser> _signInManager)
        {
            _context = context;
            userManager = _userManager;
            signInManager = _signInManager;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Tbluser Tblusers { get; set; }

        [BindProperty]
        public TblUserMap TbluserMap { get; set; }

        public string Message { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            var data = GetLogin(Tblusers);
            if (ModelState.IsValid)
            {
                if (data != null)
                {
                    var result = await signInManager.PasswordSignInAsync(data.Username, data.Password, isPersistent: false, false); //เป็นคำสั่งที่เรียกข้อมูงจาก table asp.net Identity มา login

                    if (result.Succeeded)
                    {
                        TempData["success"] = Message = "เข้าสู่ระบบสำเร็จ";

                        return RedirectToPage("/Customer/Index");
                    }
                }
                else
                {
                    TempData["message"] = Message = "Username หรือ Password ของคุณไม่ถูกต้อง";
                }
            }

            return Page();
        }
        
        public Tbluser GetLogin(Tbluser result)
        {
            string username = result.Username;
            string password = result.Password;

            Tbluser loginUsers = _context.Tblusers
                                    .Where(l => l.Username == username && l.Password == password)
                                    .FirstOrDefault();
            return loginUsers;
        }

       public bool ValLogin(string check)
       {
            var val = _context.Tblusers.Any(u => u.Username != check);
            return val;
       }
    }
}
