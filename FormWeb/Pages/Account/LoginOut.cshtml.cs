using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FormWeb.Models;
using FormWeb.ModelsMapping;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace FormWeb.Pages.LoginDefault
{
    public class LoginOutModel : PageModel
    {
        private readonly FormWeb.Models.ESHOPDBContext _context;

        private readonly UserManager<IdentityUser> userManager;

        private readonly SignInManager<IdentityUser> signInManager;

        public LoginOutModel(FormWeb.Models.ESHOPDBContext context,
                             UserManager<IdentityUser> _userManager,
                             SignInManager<IdentityUser> _signInManager)
        {
            _context = context;
            userManager = _userManager;
            signInManager = _signInManager;
        }

        public IActionResult OnGet()
        {
            signInManager.SignOutAsync();

            return RedirectToPage("/Account/Login");
        }

        [BindProperty]
        public Tbluser tbluser { get; set; }

    }
}
