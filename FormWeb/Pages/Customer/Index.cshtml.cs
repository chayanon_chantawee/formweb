﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FormWeb.Models;
using Newtonsoft.Json;
using FormWeb.ModelsMapping;
using Microsoft.Data.SqlClient;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace FormWeb.Pages.Customer
{
    public class IndexModel : PageModel
    {
        private readonly FormWeb.Models.ESHOPDBContext _context;

        private readonly UserManager<IdentityUser> userManager;

        private readonly SignInManager<IdentityUser> signInManager;

        public IndexModel(FormWeb.Models.ESHOPDBContext context,
                             UserManager<IdentityUser> _userManager,
                             SignInManager<IdentityUser> _signInManager)
        {
            _context = context;
            userManager = _userManager;
            signInManager = _signInManager;
        }

        public IList<Tblcustomer> Tblcustomer { get; set; }
        //public IList<TBLCUSTOMERMAP> TBLCUSTOMERMAP { get; set; }

        [BindProperty]
        public List<Tbljob> Tbljobs { get; set; }

        [BindProperty]
        public TBLCUSTOMERMAP Tblcusmap { get; set; }

        [BindProperty]
        public PaginationList<Tblcustomer> TBLCUSTOMERMAP { get; set; }

        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }

        [BindProperty(SupportsGet = true)]
        public int pageIndex { get; set; } = 1;

        public async Task<IActionResult> OnGetAsync()
        {
            //string json;

            //Tblcustomer = await _context.Tblcustomers.ToListAsync();

            //TBLCUSTOMERMAP = (PaginationList<TBLCUSTOMERMAP>)Tblcustomer.Cast<TBLCUSTOMERMAP>();

            //json = JsonConvert.SerializeObject(Tblcustomer);

            try
            {
                Tbljobs = await _context.Tbljobs.ToListAsync();
                //List<TBLCUSTOMERMAP> tblcustomermap = JsonConvert.DeserializeObject<List<TBLCUSTOMERMAP>>(json);

                IQueryable<Tblcustomer> tblcustomermap;

                tblcustomermap = await GetCustomer();

                if (!string.IsNullOrEmpty(SearchString))
                {
                    tblcustomermap = tblcustomermap.Where(s => s.Code.Contains(SearchString.Trim()) ||
                                                          s.Name.Contains(SearchString.Trim()) ||
                                                          s.Mobile.Contains(SearchString.Trim()));
                }

                TBLCUSTOMERMAP = await PaginationList<Tblcustomer>.CreateAsync(tblcustomermap.AsNoTracking(), pageIndex, 3);
            }
            catch (Exception ex)
            {

            }

            return Page();
        }

        //public JsonResult OnPostEdit([FromBody] TBLCUSTOMERMAP Code)
        //{
        //    List<TBLCUSTOMERMAP> Ltblmap = new List<TBLCUSTOMERMAP>();
        //    string codes = Code.Code;
        //    if (Code.Code != null)
        //    {
        //        Ltblmap = GetEdit(codes);
        //        foreach (var item in Ltblmap)
        //        {
        //            Tblcusmap.Code = item.Code;
        //            Tblcusmap.Name = item.Name;
        //            Tblcusmap.Lastname = item.Lastname;
        //            Tblcusmap.Mobile = item.Mobile;
        //            Tblcusmap.Job = item.Job;
        //            Tblcusmap.JobName = item.JobName;
        //            Tblcusmap.Sex = item.Sex;
        //            Tblcusmap.CreateDate = item.CreateDate;
        //            Tblcusmap.Status = item.Status;
        //        }
        //    }

        //    return new JsonResult(Tblcusmap);
        //}

        public async Task<IQueryable<Tblcustomer>> GetCustomer()
        {
            return await Task.Run(() =>
            {
                var qry = (from cus in _context.Set<Tblcustomer>()
                           join job in _context.Set<Tbljob>()
                           on cus.Job equals job.Jobcode
                           where cus.Status == "Y"
                           orderby cus.Code descending
                           select new Tblcustomer
                           {
                               Code = cus.Code,
                               Name = cus.Name,
                               Lastname = cus.Lastname,
                               Mobile = cus.Mobile,
                               Job = job.Jobname,
                               //JobName = job.Jobname,
                               Sex = cus.Sex,
                               CreateDate = cus.CreateDate,
                               Status = cus.Status
                           });
                return qry;
            });
        }

        public List<TBLCUSTOMERMAP> GetEdit(string Code)
        {
            List<TBLCUSTOMERMAP> tblmap = new List<TBLCUSTOMERMAP>();
            var qry = (from cus in _context.Set<Tblcustomer>()
                       join job in _context.Set<Tbljob>()
                       on cus.Job equals job.Jobcode
                       where cus.Code == Code && cus.Status == "Y"
                       select new TBLCUSTOMERMAP
                       {
                           Code = cus.Code,
                           Name = cus.Name,
                           Lastname = cus.Lastname,
                           JobName = job.Jobname,
                           Job = cus.Job,
                           Mobile = cus.Mobile,
                           CreateDate = cus.CreateDate,
                           Sex = cus.Sex,
                           Status = cus.Status
                       });
            tblmap = qry.ToList();
            return tblmap;
        }
    }
}
