﻿using FormWeb.Models;
using FormWeb.ModelsMapping;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FormWeb.Pages.Customer
{
    public class EditModel : PageModel
    {
        private readonly FormWeb.Models.ESHOPDBContext _context;

        private readonly UserManager<IdentityUser> userManager;

        private readonly SignInManager<IdentityUser> signInManager;

        public EditModel(FormWeb.Models.ESHOPDBContext context,
                             UserManager<IdentityUser> _userManager,
                             SignInManager<IdentityUser> _signInManager)
        {
            _context = context;
            userManager = _userManager;
            signInManager = _signInManager;
        }

        [BindProperty]
        public Tblcustomer Tblcustomer { get; set; }

        //public IndexModel indexModel { get; set; } หากต้องการใช้ Function จาก PageModel อื่นให้ประกาศแบบนี้

        [BindProperty]
        public List<TBLCUSTOMERMAP> Tblcustomermap { get; set; }

        [BindProperty]
        public List<Tbljob> Tbljobs { get; set; }

        public async Task<IActionResult> OnGet(string para)
        {
            Tbljobs = await _context.Tbljobs.ToListAsync();
            Tblcustomer tbl = new Tblcustomer();

            if (para == null)
            {
                return NotFound();
            }

            Tblcustomer = _context.Tblcustomers.Where(c => c.Code == para).FirstOrDefault();
            //Tblcustomermap = await GetEdit(para);

            if (Tblcustomer == null)
            {
                return NotFound();
            }

            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostSuccess()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (Tblcustomer != null)
            {
                Tblcustomer.Status = "Y";
            }

            _context.Attach(Tblcustomer).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TblcustomerExists(Tblcustomer.Code))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool TblcustomerExists(string id)
        {
            return _context.Tblcustomers.Any(e => e.Code == id);
        }

        public async Task<List<TBLCUSTOMERMAP>> GetEdit(string id)
        {
            return await Task.Run(() =>
            {
                var qry = (from cus in _context.Set<Tblcustomer>()
                           join job in _context.Set<Tbljob>()
                           on cus.Job equals job.Jobcode
                           where cus.Code == id && cus.Status == "Y"
                           select new TBLCUSTOMERMAP
                           {
                               Code = cus.Code,
                               Name = cus.Name,
                               Lastname = cus.Lastname,
                               JobName = job.Jobname,
                               Job = cus.Job,
                               Mobile = cus.Mobile,
                               CreateDate = cus.CreateDate,
                               Sex = cus.Sex,
                               Status = cus.Status
                           });

                return qry.ToListAsync();
            });
        }

        //public async Task<JsonResult> EditData([FromBody] TBLCUSTOMERMAP obj)
        //{
        //    string code = obj.Code;

        //    if (obj.Code != null)
        //    {
        //        Tblcustomermap = await GetEdit(code);
        //    }

        //    return new JsonResult(Tblcustomermap);
        //}

    }
}
