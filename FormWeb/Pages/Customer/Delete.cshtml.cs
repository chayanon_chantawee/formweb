﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FormWeb.Models;

namespace FormWeb.Pages.Customer
{
    public class DeleteModel : PageModel
    {
        private readonly FormWeb.Models.ESHOPDBContext _context;

        public DeleteModel(FormWeb.Models.ESHOPDBContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Tblcustomer Tblcustomer { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tblcustomer = await _context.Tblcustomers.FirstOrDefaultAsync(m => m.Code == id);

            if (Tblcustomer == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Tblcustomer = await _context.Tblcustomers.FindAsync(id);

            if (Tblcustomer != null)
            {
                _context.Tblcustomers.Remove(Tblcustomer);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
