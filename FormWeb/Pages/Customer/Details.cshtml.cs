﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FormWeb.Models;
using FormWeb.ModelsMapping;

namespace FormWeb.Pages.Customer
{
    public class DetailsModel : PageModel
    {
        private readonly FormWeb.Models.ESHOPDBContext _context;

        public DetailsModel(FormWeb.Models.ESHOPDBContext context)
        {
            _context = context;
        }

        public Tblcustomer Tblcustomer { get; set; }

        public List<TBLCUSTOMERMAP> Tblcustomermap { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            List<TBLCUSTOMERMAP> tblcustomermap = await GetCustomer(id);

            Tblcustomermap = tblcustomermap;

            if (Tblcustomermap == null)
            {
                return NotFound();
            }
            return Page();

        }

        public async Task<List<TBLCUSTOMERMAP>> GetCustomer(string id)
        {
            return await Task.Run(() =>
            {
                var qry = (from cus in _context.Tblcustomers
                           join job in _context.Tbljobs on cus.Job equals job.Jobcode
                           where cus.Code == id && cus.Status == "Y"
                           select new TBLCUSTOMERMAP
                           {
                               Code = cus.Code,
                               Name = cus.Name,
                               Lastname = cus.Lastname,
                               JobName = job.Jobname,
                               Job = cus.Job,
                               Mobile = cus.Mobile,
                               CreateDate = cus.CreateDate,
                               Sex = cus.Sex,
                               Status = cus.Status
                           });
                return qry.ToList();
            });
        }
    }
}
