﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using FormWeb.Models;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;

namespace FormWeb.Pages.Customer
{
    public class CreateModel : PageModel
    {
        private readonly FormWeb.Models.ESHOPDBContext _context;

        public CreateModel(FormWeb.Models.ESHOPDBContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Tblcustomer Tblcustomer { get; set; }
        [BindProperty]
        public List<Tbljob> Tbljobs { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            Tbljobs = await _context.Tbljobs.ToListAsync();
            //Tblcustomer.CreateDate = DateTime.Now;
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                int code = GetCodeCustomer();

                if (Tblcustomer.Code == null || Tblcustomer.Code == "")
                {
                    if (code >= 100)
                    {
                        Tblcustomer.Code = "CD" + code.ToString();
                    }
                    else if (code >= 10)
                    {
                        Tblcustomer.Code = "CD0" + code.ToString();
                    }
                    else
                    {
                        Tblcustomer.Code = "CD00" + code.ToString();
                    }
                }

                if (Tblcustomer.Status == null || Tblcustomer.Status == "")
                {
                    Tblcustomer.Status = "Y";
                }

                _context.Tblcustomers.Add(Tblcustomer);
                await _context.SaveChangesAsync();

                return RedirectToPage("./Index");
            }
            else
            {
                return Page();
            }

        }

        public int GetCodeCustomer() //Function Gen CodeCustomer
        {
            //string code;
            var qry = (from cus in _context.Tblcustomers select cus.Code).Count();
            var result = qry;

            return result;
        }
    }
}
