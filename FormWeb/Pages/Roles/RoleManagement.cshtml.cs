using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using FormWeb.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace FormWeb.Pages.Roles
{
    public class RoleManagementModel : PageModel
    {
        private readonly FormWeb.Models.ESHOPDBContext _context;

        private readonly RoleManager<IdentityRole> roleManager;

        public RoleManagementModel(FormWeb.Models.ESHOPDBContext context,
                                   RoleManager<IdentityRole> role)
        {
            _context = context;
            roleManager = role;
        }

        public List<TblRole> TblRoles { get; set; } //Get

        [BindProperty]
        public TblRole TblRoleEdit { get; set; }

        [BindProperty]
        public TblRole TblRolesPost { get; set; }

        public string message { get; set; }

        public async Task<IActionResult> OnGet(int para)
        {
            if(para != 0)
            {
                TblRoleEdit = await _context.TblRoles.FindAsync(para);
            }

            TblRoles = await _context.TblRoles.ToListAsync();

            if (TblRoles == null)
            {
                List<TblRole> roles = new List<TblRole>();
                roles.Add(new TblRole() { Role = "��辺������", Id = 1234 });
                TblRoles = roles;
            }

            return Page();
        }

        public async Task<IActionResult> OnPostInsertRole()
        {
            if (TblRolesPost == null)
            {
                return Page();
            }

            int tRole = _context.TblRoles.Where(r => r.Role == TblRolesPost.Role).Count();

            if (tRole > 0)
            {
                TempData["Message"] = message = "�������Է�������к�����";
                return RedirectToPage("/Roles/RoleManagement");
            }

            IdentityRole identityRole = new IdentityRole();
            {
                identityRole.Name = TblRolesPost.Role;
            }
            IdentityResult result = await roleManager.CreateAsync(identityRole);

            if (result.Succeeded)
            {
                _context.TblRoles.Add(TblRolesPost);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("/Roles/RoleManagement");

        }

        public async Task<IActionResult> OnPostRoleEdit(int editpara)
        {
            if (editpara != TblRoleEdit.Id)
            {
                TblRoles = await _context.TblRoles.ToListAsync();
                return RedirectToPage("/Roles/RoleManagement", new { para = TblRoleEdit.Id });
            }

            int tRole = _context.TblRoles.Where(r => r.Role == TblRoleEdit.Role).Count();

            if (tRole > 0)
            {
                TempData["Message"] = message = "�������Է�������к�����";
                TblRoles = await _context.TblRoles.ToListAsync();
                return RedirectToPage("/Roles/RoleManagement", new { para = TblRoleEdit.Id });
            }

            _context.Attach(TblRoleEdit).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TblRoleEditExists(TblRoleEdit.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("/Roles/RoleManagement");
        }

        private bool TblRoleEditExists(int id)
        {
            return _context.TblRoles.Any(e => e.Id == id);
        }

    }
}
