﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FormWeb.Models;
using FormWeb.ModelsMapping;
using Microsoft.AspNetCore.Identity;

namespace FormWeb.Pages.EmpInfo
{
    public class EmpDetailsModel : PageModel
    {
        private readonly FormWeb.Models.ESHOPDBContext _context;

        private readonly UserManager<IdentityUser> userManager; //Consturctor asp.net Identity For Create User

        private readonly SignInManager<IdentityUser> signInManager; //Consturctor asp.net Identity For Sign in

        public EmpDetailsModel(FormWeb.Models.ESHOPDBContext context,
                             UserManager<IdentityUser> _userManager,
                             SignInManager<IdentityUser> _signInManager)
        {
            _context = context;
            userManager = _userManager;
            signInManager = _signInManager;
        }

        #region Binding Get

        public IList<TblRole> TblRoleselect { get; set; }

        public Tblemployee Tblemployee { get; set; }

        public List<TblEmpRole> getEmpRole { get; set; }

        #endregion

        #region Binding Post

        [BindProperty]
        public TblEmpolyeemap TblEmpolyeemaps { get; set; }

        [BindProperty]
        public TblEmpRole EmpRole { get; set; }

        [BindProperty]
        public Tbluser UserRegis { get; set; }

        #endregion

        public async Task<IActionResult> OnGetAsync(int? para) // Get Emp, UserLogin, Roles
        {
            if (para == 0)
            {
                return NotFound(); //ต้องทำ Message บอกเพิ่มแล้ว Redirect กลับไปหน้า ManageEmp
            }
            else
            {
                var tblmap = await GetEmployee((int)para);


                Tblemployee = await tblmap.FirstOrDefaultAsync();
            }

            if (Tblemployee == null)
            {

                return NotFound(); //ต้องทำ Message บอกเพิ่มแล้ว Redirect กลับไปหน้า ManageEmp
            }

            bool CheckEmpUser = TblUserExists(Tblemployee.EmpCode);

            if (CheckEmpUser == true)
            {
                var qyrUser = _context.Tblusers.AsQueryable().Where(u => u.EmpCode == Tblemployee.EmpCode);

                if (qyrUser != null)
                {
                    UserRegis = await qyrUser.FirstOrDefaultAsync();
                }

                var qryEmprole = _context.TblEmpRoles.AsQueryable().Where(er => er.EmpCode == Tblemployee.EmpCode);

                if (qryEmprole != null)
                {
                    getEmpRole = await qryEmprole.ToListAsync();
                }
            }

            TblRoleselect = await _context.TblRoles.ToListAsync();

            return Page();
        }

        public async Task<IActionResult> OnPostRegisterAndEdit(string Codepara, int Idpara) //Post ส่วนของ Register
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (UserRegis.Password != UserRegis.ConfirmPassword)
            {
                return RedirectToPage("/EmpInfo/EmpDetails", new { para = Idpara });
            }

            var checkEmp = TblEmpCheck(Codepara);
            var checkUserLogin = TblUserExists(Codepara);

            if (checkEmp == true)
            {
                if(checkUserLogin == false) //Insert
                {
                    IdentityUser user = new IdentityUser { UserName = UserRegis.Username, PasswordHash = UserRegis.Password };

                    var result = await userManager.CreateAsync(user, UserRegis.Password); // Insert User

                    if (result.Succeeded) // ต้องเข้าเงื่อนไขของการสร้างรหัสถึงจะ สามารถบันทึกลง base ได้
                    {
                        if (UserRegis.EmpCode == null )
                        {
                            UserRegis.EmpCode = Codepara;
                        }

                        _context.Tblusers.Add(UserRegis);
                        await _context.SaveChangesAsync();

                        return RedirectToPage("/EmpInfo/EmpDetails", new { para = Idpara });
                    } 
                }

                //else //Update
                //{                   

                //    IdentityUser user = new IdentityUser { UserName = UserRegis.Username, PasswordHash = UserRegis.Password };

                //    var result = await userManager.get // Update User

                //    if (result.Succeeded) // ต้องเข้าเงื่อนไขของการสร้างรหัสถึงจะ สามารถบันทึกลง base ได้
                //    {

                //        _context.Attach(UserRegis).State = EntityState.Modified;
                //        await _context.SaveChangesAsync();

                //        return RedirectToPage("/EmpInfo/EmpDetails", new { para = Idpara });
                //    }

                //}
                
            }

            return RedirectToPage("/EmpInfo/EmpDetails", new { para = Idpara });
        }

        public async Task<IActionResult> OnPostAddRole(string para, int Idpara) // Add Roles
        {
            TblEmpRole empRole = new TblEmpRole();

            if (para == null && para == "")
            {
                return RedirectToPage("/EmpInfo/EmpDetails", new { para = Idpara });
            }

            bool checkEmp = TblEmpCheck(para); 
            if (checkEmp == true)
            {
                if (EmpRole.RoleId != null)
                {
                    TblRole role = new TblRole();
                    int roleId = int.Parse(EmpRole.RoleId);

                    var getRole = _context.TblRoles.AsQueryable();

                    role = await _context.TblRoles.Where(r => r.Id == roleId).FirstOrDefaultAsync();

                    if (role != null)
                    {
                        EmpRole.EmpCode = para;
                        EmpRole.RoleId = role.Id.ToString();
                        EmpRole.RoleName = role.Role;

                        _context.TblEmpRoles.Add(EmpRole);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        return RedirectToPage("/EmpInfo/EmpDetails", new { para = Idpara });
                    }
                }
                else
                {
                    return RedirectToPage("/EmpInfo/EmpDetails", new { para = Idpara });
                }
            }

            return RedirectToPage("/EmpInfo/EmpDetails", new { para = Idpara });

        }

        public async Task<IActionResult> OnPostDeleteRole(int id, int Idpara)
        {
            if(id == 0)
            {
                return RedirectToPage("/EmpInfo/EmpDetails", new { para = Idpara });
            }

            TblEmpRole emprole = new TblEmpRole();
            emprole = TbldeleteRole(id);

            if(emprole != null)
            {
                _context.TblEmpRoles.Remove(emprole);

                await _context.SaveChangesAsync();
            }
            else
            {
                return RedirectToPage("/EmpInfo/EmpDetails", new { para = Idpara });
            }

            return RedirectToPage("/EmpInfo/EmpDetails", new { para = Idpara });

        } //DeleteRole

        private TblEmpRole TbldeleteRole(int id)
        {
            var qry = _context.TblEmpRoles.AsQueryable().Where(role => role.Id == id);

            return qry.FirstOrDefault();
        }

        private bool TblUserExists(string para) //Check User data have Empcode for UserLogin
        {
            var qry = _context.Tblusers.AsQueryable().Any(user => user.EmpCode == para);
            return qry;
        }

        private bool TblEmpCheck(string Codepara) //Check Paramrter EmpCode if Fine will Use For Insert to TblUser
        {
            var qry = _context.Tblemployees.AsQueryable().Any(e => e.EmpCode == Codepara);
            return qry;
        }

        public async Task<IQueryable<Tblemployee>> GetEmployee(int para)  //Query map ข้อมูล
        {
            return await Task.Run(() =>
            {
                var qry = from emp in _context.Set<Tblemployee>()
                          join dep in _context.Set<Tbldepartment>()
                          on emp.DepartmentCode equals dep.DepartmentCode
                          into Dep
                          from dep in Dep.DefaultIfEmpty()
                          join pst in _context.Set<Tblposition>()
                          on emp.Position equals pst.PositionCode
                          into Pot
                          from pst in Pot.DefaultIfEmpty()
                          where emp.Id == para && emp.Status == "Y"
                          select new Tblemployee
                          {
                              Id = emp.Id,
                              EmpCode = emp.EmpCode,
                              EmpName = emp.EmpName,
                              EmpLastName = emp.EmpLastName,
                              Email = emp.Email,
                              Mobile = emp.Mobile,
                              DepartmentCode = emp.DepartmentCode,
                              Department = dep == null ? "" : dep.DepartmentName,
                              Position = emp.Position,
                              PositioN = pst == null ? "" : pst.PositionName,
                              Status = emp.Status,
                              CreateDate = emp.CreateDate
                          };

                return qry;
            });
        }
    }
}
