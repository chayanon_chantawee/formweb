﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using FormWeb.Models;
using FormWeb.ModelsMapping;

namespace FormWeb.Pages.EmpInfo
{
    public class ManageEmpModel : PageModel
    {
        private readonly FormWeb.Models.ESHOPDBContext _context;

        public ManageEmpModel(FormWeb.Models.ESHOPDBContext context)
        {
            _context = context;
        }

        [BindProperty]
        public PaginationList<TblEmpolyeemap> Tblempmap { get; set; }

        public Tblemployee Tblemployee { get; set; }

        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }

        [BindProperty(SupportsGet = true)]
        public int pageIndex { get; set; } = 1;

        public async Task<IActionResult> OnGetAsync()
        {
            IQueryable<TblEmpolyeemap> tblEmpMqpQry;
            tblEmpMqpQry = await GetEmp();

            if (!string.IsNullOrEmpty(SearchString))
            {
                tblEmpMqpQry = tblEmpMqpQry.Where(s => s.EmpCode.Contains(SearchString.Trim()) ||
                                                  s.EmpName.Contains(SearchString.Trim()) ||
                                                  s.EmpLastName.Contains(SearchString.Trim()) ||
                                                  s.Email.Contains(SearchString.Trim()) ||
                                                  s.PositionName.Contains(SearchString.Trim()));
            }

            Tblempmap = await PaginationList<TblEmpolyeemap>.CreateAsync(tblEmpMqpQry.AsNoTracking(), pageIndex, 3);
            return Page();
        }

        public async Task<IQueryable<TblEmpolyeemap>> GetEmp()
        {
            return await Task.Run(() =>
            {
                var qry = (from emp in _context.Tblemployees
                           join dpm in _context.Tbldepartments
                           on emp.DepartmentCode equals dpm.DepartmentCode
                           join pst in _context.Tblpositions
                           on emp.Position equals pst.PositionCode
                           where emp.Status == "Y"
                           orderby emp.Id descending
                           select new TblEmpolyeemap
                           {
                               Id = emp.Id,
                               EmpCode = emp.EmpCode,
                               EmpName = emp.EmpName,
                               EmpLastName = emp.EmpLastName,
                               Mobile = emp.Mobile,
                               Email = emp.Email,
                               DepartmentCode = emp.DepartmentCode,
                               DepartmentName = dpm.DepartmentName,
                               Position = emp.Position,
                               PositionName = pst.PositionName,
                               Status = "Y",
                               CreateDate = emp.CreateDate
                           });
                return qry;
            });
        }
    }
}
