﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FormWeb.Models;
using FormWeb.ModelsMapping;
using Newtonsoft.Json;

namespace FormWeb.Pages.EmpInfo
{
    public class EditEmpModel : PageModel
    {
        private readonly FormWeb.Models.ESHOPDBContext _context;

        public EditEmpModel(FormWeb.Models.ESHOPDBContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Tblemployee Tblemployees { get; set; }

        [BindProperty]
        public TblEmpolyeemap TblEmpolyeemaps { get; set; }

        public IList<Tblposition> Tblposition { get; set; }

        public IList<Tbldepartment> Tbldepartment { get; set; }

        public async Task<IActionResult> OnGetAsync(int para)
        {
            Tbldepartment = await _context.Tbldepartments.ToListAsync();
            Tblposition = await _context.Tblpositions.ToListAsync();

            var employees = _context.Tblemployees.Where(e => e.Id == para).AsQueryable();

            Tblemployees = await employees.FirstOrDefaultAsync();

            if (Tblemployees == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            Tblemployee empobj = new Tblemployee();

            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (TblEmpolyeemaps != null)
            {
                TblEmpolyeemaps.Status = "Y";

                string json = JsonConvert.SerializeObject(TblEmpolyeemaps);

                empobj = JsonConvert.DeserializeObject<Tblemployee>(json);
            }
            Tblemployees = empobj;

            _context.Attach(Tblemployees).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TblemployeeExists(Tblemployees.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./ManageEmp");
        }

        private bool TblemployeeExists(int id)
        {
            return _context.Tblemployees.Any(e => e.Id == id);
        }

        //public async Task<List<TblEmpolyeemap>> GetEmployee(int para)
        //{
        //    return await Task.Run(() =>
        //    {
        //        var qry = from emp in _context.Set<Tblemployee>()
        //                  join dep in _context.Set<Tbldepartment>()
        //                  on emp.DepartmentCode equals dep.DepartmentCode
        //                  join pst in _context.Set<Tblposition>()
        //                  on emp.Position equals pst.PositionCode
        //                  where emp.Id == para && emp.Status == "Y"
        //                  select new TblEmpolyeemap
        //                  {
        //                      Id = emp.Id,
        //                      EmpCode = emp.EmpCode,
        //                      EmpName = emp.EmpName,
        //                      EmpLastName = emp.EmpLastName,
        //                      Email = emp.Email,
        //                      Mobile = emp.Mobile,
        //                      DepartmentCode = emp.DepartmentCode,
        //                      DepartmentName = dep.DepartmentName,
        //                      Position = emp.Position,
        //                      PositionName = pst.PositionName,
        //                      Role = emp.Role,
        //                      Status = emp.Status,
        //                      CreateDate = emp.CreateDate
        //                  };

        //        return qry.ToListAsync();
        //    });
        //}
    }
}
