﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using FormWeb.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace FormWeb.Pages.EmpInfo
{
    public class CreateEmpModel : PageModel
    {
        private readonly FormWeb.Models.ESHOPDBContext _context;

        public CreateEmpModel(FormWeb.Models.ESHOPDBContext context)
        {
            _context = context;
        }

        public async Task OnGetAsync()
        {
            Tbldepartment = await _context.Tbldepartments.ToListAsync();
            Tblposition = await _context.Tblpositions.ToListAsync();
        }

        public ManageEmpModel manageEmp { get; set; }

        [BindProperty]
        public Tblemployee Tblemployee { get; set; }
        [BindProperty]
        public List<Tbldepartment> Tbldepartment { get; set; }
        [BindProperty]
        public List<Tblposition> Tblposition { get; set; }

        public string Message { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                if(Tblemployee.Id <= 0)
                {
                    InsertEmp(Tblemployee);

                    Tblemployee.Status = "Y";

                    _context.Tblemployees.Add(Tblemployee);

                    await _context.SaveChangesAsync();

                    return RedirectToPage("/EmpInfo/ManageEmp");
                }
                //HttpContext.Session.SetString("SessionEmp", JsonConvert.SerializeObject(Tblemployee)); /*Session For object*/
                TempData["Message"] = Message = "กรุณาลองใหม่อีกครั้ง";

                return RedirectToPage("/EmpInfo/ManageEmp");
            }

            return Page();

        }

        public void InsertEmp(Tblemployee empCode)
        {
            string dt = DateTime.Now.Ticks.ToString();

            string Code = "EP" + dt.Substring(dt.Length - 5, 5);

            empCode.EmpCode = Code;
        }
    }

}
