﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FormWeb
{
    public class Helper
    {
        public static string RenderRazorViewToString(PageModel pageModel , string viewName , object model = null)
        {
            pageModel.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                IViewEngine viewEngine = pageModel.HttpContext.RequestServices.GetService(typeof(ICompositeViewEngine)) as ICompositeViewEngine;
                ViewEngineResult viewEngineResult = viewEngine.FindView(pageModel.PageContext, viewName, false);

                ViewContext viewContext = new ViewContext
                (
                    pageModel.PageContext,
                    viewEngineResult.View,
                    pageModel.ViewData,
                    pageModel.TempData,
                    sw,
                    new HtmlHelperOptions()
                );

                viewEngineResult.View.RenderAsync(viewContext);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
}
