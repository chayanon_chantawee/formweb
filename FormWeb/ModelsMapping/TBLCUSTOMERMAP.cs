﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FormWeb.ModelsMapping
{
    public partial class TBLCUSTOMERMAP
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string Mobile { get; set; }
        public string Job { get; set; }
        public string JobName { get; set; }
        public string Sex { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? CreateDate { get; set; }
        public string Status { get; set; }
    }
}
