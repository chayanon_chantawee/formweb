﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FormWeb.ModelsMapping
{
    public partial class TblUserMap
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public string EmpCode { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
    }
}
