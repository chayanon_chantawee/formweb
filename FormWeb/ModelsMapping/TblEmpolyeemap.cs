﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FormWeb.ModelsMapping
{
    public partial class TblEmpolyeemap
    {
        public int Id { get; set; }
        [Display(Name = "รหัสพนักงาน")]
        public string EmpCode { get; set; }
        [Display(Name = "ชื่อ")]
        public string EmpName { get; set; }
        [Display(Name = "นามสกุล")]
        public string EmpLastName { get; set; }
        [Display(Name = "เบอร์โทรศัพท์")]
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string DepartmentCode { get; set; }
        [Display(Name = "แผนก")]
        public string DepartmentName { get; set; }
        public string Role { get; set; }
        public string Position { get; set; }
        [Display(Name = "ตำแหน่ง")]
        public string PositionName { get; set; }
        public string Status { get; set; }
        [Display(Name = "วันที่เริ่มงาน")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? CreateDate { get; set; }
    }
}
