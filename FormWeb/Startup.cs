using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FormWeb.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Serialization;

namespace FormWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            //services.AddMvc()
            //        .SetCompatibilityVersion(CompatibilityVersion.Latest)
            //        .AddJsonOptions(options => options.JsonSerializerOptions.DictionaryKeyPolicy = new ;
            services.AddAntiforgery(o => o.HeaderName = "XSRF-TOKEN");
            services.AddSession();
            services.AddMvc().AddRazorPagesOptions(options =>
            {
                options.Conventions.AddPageRoute("/Customer/Index","");
            });

            var connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ESHOPDBContext>(options => options.UseSqlServer(connection));

            services.AddIdentity<IdentityUser, IdentityRole>(options =>
            {
                options.Password.RequiredLength = 8;
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;

            }).AddEntityFrameworkStores<ESHOPDBContext>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseSession();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
/*options =>
            {
                options.Conventions.AuthorizeFolder("/Pages/LoginDefault");
                options.Conventions.AuthorizePage("/LoginPage");
                //options.Conventions.AuthorizeFolder("/LoginDefault");
            });*/

//services.Configure<IdentityOptions>(options =>
//{
//    options.Cookies.ApplicationCookie.LoginPath = new PathString("/login");
//});