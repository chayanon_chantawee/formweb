﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FormWeb.Models
{
    public partial class TblEmpRole
    {
        public int Id { get; set; }
        public string EmpCode { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
