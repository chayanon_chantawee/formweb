﻿using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using FormWeb.ModelsMapping;

#nullable disable

namespace FormWeb.Models
{
    public partial class ESHOPDBContext : IdentityDbContext
    {
        public ESHOPDBContext()
        {
        }

        public ESHOPDBContext(DbContextOptions<ESHOPDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Tblcustomer> Tblcustomers { get; set; }
        public virtual DbSet<Tbltest> Tbltests { get; set; }
        public virtual DbSet<Tbluser> Tblusers { get; set; }
        public virtual DbSet<Tbljob> Tbljobs { get; set; }
        public virtual DbSet<Tblemployee> Tblemployees { get; set; }
        public virtual DbSet<Tbldepartment> Tbldepartments { get; set; }
        public virtual DbSet<Tblposition> Tblpositions { get; set; }
        public virtual DbSet<TblRole> TblRoles { get; set; }

        public virtual DbSet<TblEmpRole> TblEmpRoles { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=DESKTOP-HSJ9R78\\SQLEXPRESS;Initial Catalog=ESHOPDB;User ID=sa;Password=GainChayanoN;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Thai_CI_AS")
                        .UseIdentityColumns()
                        .HasAnnotation("ProductVersion", "5.0.1");

            modelBuilder.Entity<Tblcustomer>(entity =>
            {
                entity.HasKey(e => e.Code);

                entity.ToTable("TBLCUSTOMER");

                entity.Property(e => e.Code)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("CODE");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasColumnName("CREATE_DATE");

                entity.Property(e => e.Job)
                    .HasMaxLength(150)
                    .HasColumnName("JOB");

                //entity.Property(e => e.JobName)
                //    .HasMaxLength(150);

                entity.Property(e => e.Lastname)
                    .HasMaxLength(150)
                    .HasColumnName("LASTNAME");

                entity.Property(e => e.Mobile)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .HasColumnName("MOBILE");

                entity.Property(e => e.Name)
                    .HasMaxLength(150)
                    .HasColumnName("NAME");

                entity.Property(e => e.Sex)
                    .HasMaxLength(10)
                    .HasColumnName("SEX");

                entity.Property(e => e.Status)
                    .HasMaxLength(10)
                    .HasColumnName("STATUS");
            });

            modelBuilder.Entity<TblEmpRole>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("TblEmpRole");

                entity.Property(e => e.EmpCode).HasMaxLength(20).HasColumnName("EmpCode");

                entity.Property(e => e.RoleId).HasMaxLength(20).HasColumnName("RoleId");

                entity.Property(e => e.RoleName).HasMaxLength(20).HasColumnName("RoleName");
            });


            modelBuilder.Entity<TblRole>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("TblRoles");

                entity.Property(e => e.Role).HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("Role"); ;
            });

            modelBuilder.Entity<Tbldepartment>(entity =>
                {
                    entity.HasKey(e => e.DepartmentId);

                    entity.ToTable("TBLDEPARTMENT");

                    entity.Property(e => e.DepartmentCode).HasMaxLength(50).HasColumnName("DepartmentCode");

                    entity.Property(e => e.DepartmentName).HasMaxLength(50).HasColumnName("DepartmentName");
                });

            modelBuilder.Entity<Tblposition>(entity =>
            {
                entity.HasKey(e => e.PositionId);

                entity.ToTable("TBLPOSITION");

                entity.Property(e => e.PositionCode).HasMaxLength(50).HasColumnName("PositionCode");

                entity.Property(e => e.PositionName).HasMaxLength(50).HasColumnName("PositionName");
            });

            modelBuilder.Entity<Tbltest>(entity =>
            {
                entity.ToTable("TBLTEST");

                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("ID");

                entity.Property(e => e.Lastname).HasMaxLength(150);

                entity.Property(e => e.Name).HasMaxLength(150);
            });

            modelBuilder.Entity<Tbluser>(entity =>
            {
                entity.HasKey(e => e.Username);

                entity.ToTable("TBLUSER");

                entity.Property(e => e.Username)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("USERNAME");

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("PASSWORD");

                entity.Property(e => e.EmpCode)
                   .HasMaxLength(50)
                   .IsUnicode(false)
                   .HasColumnName("EMPCODE");
            });

            modelBuilder.Entity<Tbljob>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("TBLJob");

                entity.Property(e => e.Jobcode)
                    .HasMaxLength(20)
                    .HasColumnName("JOBCode");

                entity.Property(e => e.Jobname)
                    .HasMaxLength(50)
                    .HasColumnName("JOBName");
            });

            modelBuilder.Entity<Tblemployee>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("TBLEMPLOYEE");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasColumnName("CreateDate");

                entity.Property(e => e.DepartmentCode)
                    .HasMaxLength(50)
                    .HasColumnName("DepartmentCode");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("Email");

                entity.Property(e => e.EmpCode)
                    .HasMaxLength(15)
                    .HasColumnName("EmpCode");

                entity.Property(e => e.EmpLastName)
                    .HasMaxLength(50)
                    .HasColumnName("EmpLastName");

                entity.Property(e => e.EmpName)
                    .HasMaxLength(50)
                    .HasColumnName("EmpName");

                entity.Property(e => e.Mobile)
                    .HasMaxLength(50)
                    .HasColumnName("Mobile");

                entity.Property(e => e.Position)
                    .HasMaxLength(50)
                    .HasColumnName("Position");

                entity.Property(e => e.Status)
                    .HasMaxLength(10)
                    .HasColumnName("Status");
            });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
            {
                b.Property<string>("Id")
                    .HasColumnType("nvarchar(450)");

                b.Property<string>("ConcurrencyStamp")
                    .IsConcurrencyToken()
                    .HasColumnType("nvarchar(max)");

                b.Property<string>("Name")
                    .HasMaxLength(256)
                    .HasColumnType("nvarchar(256)");

                b.Property<string>("NormalizedName")
                    .HasMaxLength(256)
                    .HasColumnType("nvarchar(256)");

                b.HasKey("Id");

                b.HasIndex("NormalizedName")
                    .IsUnique()
                    .HasDatabaseName("RoleNameIndex")
                    .HasFilter("[NormalizedName] IS NOT NULL");

                b.ToTable("AspNetRoles");
            });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
            {
                b.Property<int>("Id")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("int")
                    .UseIdentityColumn();

                b.Property<string>("ClaimType")
                    .HasColumnType("nvarchar(max)");

                b.Property<string>("ClaimValue")
                    .HasColumnType("nvarchar(max)");

                b.Property<string>("RoleId")
                    .IsRequired()
                    .HasColumnType("nvarchar(450)");

                b.HasKey("Id");

                b.HasIndex("RoleId");

                b.ToTable("AspNetRoleClaims");
            });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUser", b =>
            {
                b.Property<string>("Id")
                    .HasColumnType("nvarchar(450)");

                b.Property<int>("AccessFailedCount")
                    .HasColumnType("int");

                b.Property<string>("ConcurrencyStamp")
                    .IsConcurrencyToken()
                    .HasColumnType("nvarchar(max)");

                b.Property<string>("Email")
                    .HasMaxLength(256)
                    .HasColumnType("nvarchar(256)");

                b.Property<bool>("EmailConfirmed")
                    .HasColumnType("bit");

                b.Property<bool>("LockoutEnabled")
                    .HasColumnType("bit");

                b.Property<DateTimeOffset?>("LockoutEnd")
                    .HasColumnType("datetimeoffset");

                b.Property<string>("NormalizedEmail")
                    .HasMaxLength(256)
                    .HasColumnType("nvarchar(256)");

                b.Property<string>("NormalizedUserName")
                    .HasMaxLength(256)
                    .HasColumnType("nvarchar(256)");

                b.Property<string>("PasswordHash")
                    .HasColumnType("nvarchar(max)");

                b.Property<string>("PhoneNumber")
                    .HasColumnType("nvarchar(max)");

                b.Property<bool>("PhoneNumberConfirmed")
                    .HasColumnType("bit");

                b.Property<string>("SecurityStamp")
                    .HasColumnType("nvarchar(max)");

                b.Property<bool>("TwoFactorEnabled")
                    .HasColumnType("bit");

                b.Property<string>("UserName")
                    .HasMaxLength(256)
                    .HasColumnType("nvarchar(256)");

                b.HasKey("Id");

                b.HasIndex("NormalizedEmail")
                    .HasDatabaseName("EmailIndex");

                b.HasIndex("NormalizedUserName")
                    .IsUnique()
                    .HasDatabaseName("UserNameIndex")
                    .HasFilter("[NormalizedUserName] IS NOT NULL");

                b.ToTable("AspNetUsers");
            });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
            {
                b.Property<int>("Id")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("int")
                    .UseIdentityColumn();

                b.Property<string>("ClaimType")
                    .HasColumnType("nvarchar(max)");

                b.Property<string>("ClaimValue")
                    .HasColumnType("nvarchar(max)");

                b.Property<string>("UserId")
                    .IsRequired()
                    .HasColumnType("nvarchar(450)");

                b.HasKey("Id");

                b.HasIndex("UserId");

                b.ToTable("AspNetUserClaims");
            });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
            {
                b.Property<string>("LoginProvider")
                    .HasColumnType("nvarchar(450)");

                b.Property<string>("ProviderKey")
                    .HasColumnType("nvarchar(450)");

                b.Property<string>("ProviderDisplayName")
                    .HasColumnType("nvarchar(max)");

                b.Property<string>("UserId")
                    .IsRequired()
                    .HasColumnType("nvarchar(450)");

                b.HasKey("LoginProvider", "ProviderKey");

                b.HasIndex("UserId");

                b.ToTable("AspNetUserLogins");
            });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
            {
                b.Property<string>("UserId")
                    .HasColumnType("nvarchar(450)");

                b.Property<string>("RoleId")
                    .HasColumnType("nvarchar(450)");

                b.HasKey("UserId", "RoleId");

                b.HasIndex("RoleId");

                b.ToTable("AspNetUserRoles");
            });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
            {
                b.Property<string>("UserId")
                    .HasColumnType("nvarchar(450)");

                b.Property<string>("LoginProvider")
                    .HasColumnType("nvarchar(450)");

                b.Property<string>("Name")
                    .HasColumnType("nvarchar(450)");

                b.Property<string>("Value")
                    .HasColumnType("nvarchar(max)");

                b.HasKey("UserId", "LoginProvider", "Name");

                b.ToTable("AspNetUserTokens");
            });


            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
            {
                b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                    .WithMany()
                    .HasForeignKey("RoleId")
                    .OnDelete(DeleteBehavior.Cascade)
                    .IsRequired();
            });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
            {
                b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser", null)
                    .WithMany()
                    .HasForeignKey("UserId")
                    .OnDelete(DeleteBehavior.Cascade)
                    .IsRequired();
            });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
            {
                b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser", null)
                    .WithMany()
                    .HasForeignKey("UserId")
                    .OnDelete(DeleteBehavior.Cascade)
                    .IsRequired();
            });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
            {
                b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                    .WithMany()
                    .HasForeignKey("RoleId")
                    .OnDelete(DeleteBehavior.Cascade)
                    .IsRequired();

                b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser", null)
                    .WithMany()
                    .HasForeignKey("UserId")
                    .OnDelete(DeleteBehavior.Cascade)
                    .IsRequired();
            });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
            {
                b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser", null)
                    .WithMany()
                    .HasForeignKey("UserId")
                    .OnDelete(DeleteBehavior.Cascade)
                    .IsRequired();
            });


            OnModelCreatingPartial(modelBuilder);
        }



#pragma warning restore 612, 618

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);



#pragma warning restore 612, 618

        public DbSet<FormWeb.ModelsMapping.TblEmpolyeemap> TblEmpolyeemap { get; set; }
    }
}
