﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FormWeb.Models
{
    public partial class Tbljob
    {
        public int Id { get; set; }
        public string Jobcode { get; set; }
        public string Jobname { get; set; }
    }
}
