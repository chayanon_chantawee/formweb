﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FormWeb.Models
{
    public partial class Tblposition
    {
        public int PositionId { get; set; }
        public string PositionCode { get; set; }
        public string PositionName { get; set; }
    }
}
