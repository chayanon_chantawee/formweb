﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FormWeb.Models.LoginUser
{
    public partial class LoginUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }

}
