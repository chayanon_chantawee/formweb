﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FormWeb.Models
{
    public partial class TblRole
    {
        public int Id { get; set; }
        public string Role { get; set; }
    }
}
