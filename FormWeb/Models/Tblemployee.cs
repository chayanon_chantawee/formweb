﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace FormWeb.Models
{
    public partial class Tblemployee
    {
        public int Id { get; set; }

        [Display(Name = "รหัสพนักงาน")]
        public string EmpCode { get; set; }

        [Display(Name = "ชื่อ")]
        [Required(ErrorMessage = "กรูณากรอกชื่อ")]
        public string EmpName { get; set; }

        [Display(Name = "นามสกุล")]
        [Required(ErrorMessage = "กรุณากรอกนามสกุล")]
        public string EmpLastName { get; set; }

        [Display(Name = "เบอร์โทรศัพท์")]
        [Required(ErrorMessage = "กรุณากรอกเบอร์โทร")]
        public string Mobile { get; set; }

        [Display(Name = "E-Mail")]
        [Required(ErrorMessage = "กรุณากรอก E-mail")]
        public string Email { get; set; }
        [Required(ErrorMessage = "กรุณาเลือกแผนก")]
        public string DepartmentCode { get; set; }

        [Required(ErrorMessage = "กรุณาเลือกแผนก")]
        public string Position { get; set; }
        public string Status { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime? CreateDate { get; set; }
        
        [NotMapped]
        [Display(Name = "แผนก")]
        public string Department { get; set; }
        [NotMapped]
        [Display(Name = "ตำแหน่ง")]
        public string PositioN { get; set; }
    }
}
