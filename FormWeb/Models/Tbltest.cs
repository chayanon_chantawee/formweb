﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FormWeb.Models
{
    public partial class Tbltest
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
    }
}
